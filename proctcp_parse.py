#!/usr/bin/python3
import sys
import ipaddress
import re
import os


def parseip(ipandport: str):
    hexaddr = 0
    x = 3
    ipaddr = ipandport.split(":")[0]
    try:
        port = ipandport.split(":")[1]
    except IndexError as ie:
        port = 0
    for i in range(len(ipaddr) - 2, -1, -2):
        hexaddr += int(ipaddr[i:i + 2], 16) * pow(256, x)
        x -= 1
    port = int(port, 16)
    return ipaddress.IPv4Address(hexaddr).__str__() + ":" + str(port)


pattern = re.compile(r"^\s*(\d+:)\s+([^\s]+)\s+([^\s]+)", re.VERBOSE)

os.system("stty -echo")

if len(sys.argv) == 2:
    print(parseip(sys.argv[1]))
else:
    for line in sys.stdin:
        match = pattern.match(line)
        if match:
            print(match.group(1) + " " + parseip(match.group(2)) + " " + parseip(match.group(3)))
